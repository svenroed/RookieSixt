//
//  AppDelegate.h
//  RookieSixt
//
//  Created by Martin Kapfhammer on 27.06.12.
//  Copyright (c) 2012 Sixt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
